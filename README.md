# dwm: dynamic window manager
![desktop](desktop.png)

dwm is an extremely fast, small, and dynamic window manager for X.

## patches

- [fullgaps](https://dwm.suckless.org/patches/fullgaps/dwm-fullgaps-20200508-7b77734.diff). enable gaps between windows
- [swallow](https://dwm.suckless.org/patches/swallow/dwm-swallow-20201211-61bb8b2.diff). enable window swallowing
- [xrdb](https://dwm.suckless.org/patches/xrdb/dwm-xrdb-6.2.diff). allows dwm to read colors from xrdb
- [titlecolor](https://dwm.suckless.org/patches/titlecolor/dwm-titlecolor-20210815-ed3ab6b4.diff). adds a new color scheme used by the window title in the bar
- [center](https://dwm.suckless.org/patches/center/dwm-center-6.2.diff). automatically center clients
