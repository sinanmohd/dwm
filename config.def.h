/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int gappx     = 5;        /* gaps between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int swallowfloating    = 0;        /* 1 means swallow floating windows by default */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "Product Sans:size=12:antialias=true:autohint=true",
                                        "TerminessTTF Nerd Font:pixelsize=13:antialias=true:autohint=true",
					"Noto Color Emoji:pixelsize=12:antialias=true:autohint=true"};
static const char dmenufont[]       = "Product Sans:size=12:antialias=true:autohint=true";
static char normbgcolor[]           = "#222222";
static char normbordercolor[]       = "#444444";
static char normfgcolor[]           = "#bbbbbb";
static char selfgcolor[]            = "#eeeeee";
static char selbordercolor[]        = "#005577";
static char selbgcolor[]            = "#005577";
static char *colors[][3] = {
       /*                 fg           bg           border   */
       [SchemeNorm]   = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]    = { selfgcolor,  selbgcolor,  selbordercolor  },
       [SchemeTitle]  = { normfgcolor, normbgcolor, normbordercolor },
};

/* keysym */
static const int XK_XF86AudioMute         = 0x1008ff12;
static const int XK_XF86AudioLowerVolume  = 0x1008ff11;
static const int XK_XF86AudioRaiseVolume  = 0x1008ff13;
static const int XK_XF86AudioMicMute      = 0x1008ffb2;
static const int XK_XF86MonBrightnessDown = 0x1008ff03;
static const int XK_XF86MonBrightnessUp   = 0x1008ff02;
static const int XK_XF86Calculator        = 0x1008ff1d;
static const int XK_XF86AudioPlay         = 0x1008ff14;
static const int XK_XF86AudioStop         = 0x1008ff15;
static const int XK_XF86AudioPrev         = 0x1008ff16;
static const int XK_XF86AudioNext         = 0x1008ff17;

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class     instance  title           tags mask  iscentered  isfloating  isterminal  noswallow  monitor */
	{ "Firefox", NULL,     NULL,           1 << 8,	  0,	     0,          0,          -1,        -1 },
	{ "St",      NULL,     NULL,           0,	  0,         0,          1,           0,        -1 },
	{ NULL,      NULL,     "Event Tester", 0,	  0,         0,          0,           1,        -1 }, /* xev */
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[]    = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbordercolor, "-sf", selfgcolor, NULL };
static const char *termcmd[]     = { "st", NULL };
static const char *scrnlock[]    = { "slock", NULL };
static const char *mem_sleep[]   = { "msleep", NULL };
static const char *scrnshot[]    = { "scst", NULL };
static const char *scrnshotsel[] = { "scst", "s", NULL };
static const char *nnnfm[]       = { "st", "nnn", "-decC", NULL };
static const char *bluctl[]      = { "bluctl", NULL };
static const char *damb[]        = { "damb", NULL };
static const char *web[]         = { "tabbed", "-c", "surf", "-e", NULL };
static const char *webalt[]      = { "firefox", NULL };
static const char *ncmpcpp[]     = { "st", "ncmpcpp-ueberzug", NULL };
static const char *ytsearch[]    = { "st", "ytfzf", "-tl", NULL };
static const char *ytsub[]       = { "st", "ytfzf", "-tl", "--subs=4", NULL };
static const char *sticker[]     = { "stikman", NULL };

/* audio control */
static const char *upbri[]       = { "brictl", "-i", "1",   NULL };
static const char *downbri[]     = { "brictl", "-d", "1",   NULL };
static const char *upvol[]       = { "pamixer", "-i", "5",   NULL };
static const char *downvol[]     = { "pamixer", "-d", "5",   NULL };
static const char *mutevol[]     = { "pamixer", "-t", NULL };
static const char *mutemic[]     = { "mutemic", NULL };
static const char *audiotoggle[] = { "mctl", "toggle", NULL };
static const char *audiokill[]   = { "mctl", "kill", NULL };
static const char *audioprev[]   = { "mctl", "prev", NULL };
static const char *audionext[]   = { "mctl", "next", NULL };

static const Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	{ MODKEY,                       XK_F5,     xrdb,           {.v = NULL } },
	{ MODKEY,                       XK_minus,  setgaps,        {.i = -1 } },
	{ MODKEY,                       XK_equal,  setgaps,        {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_equal,  setgaps,        {.i = 0  } },
	{ 0,                            XK_XF86MonBrightnessUp,      spawn,          {.v = upbri   } },
	{ 0,                            XK_XF86MonBrightnessDown,    spawn,          {.v = downbri } },
	{ 0,                            XK_XF86AudioRaiseVolume,     spawn,          {.v = upvol   } },
	{ 0,                            XK_XF86AudioLowerVolume,     spawn,          {.v = downvol } },
	{ 0,                            XK_XF86AudioMute,            spawn,          {.v = mutevol } },
        { 0,                            XK_XF86AudioMicMute,         spawn,          {.v = mutemic } },
        { 0,                            XK_XF86AudioPlay,            spawn,          {.v = audiotoggle } },
	{ 0,                            XK_XF86AudioStop,            spawn,          {.v = audiokill } },
	{ 0,                            XK_XF86AudioPrev,            spawn,          {.v = audioprev } },
        { 0,                            XK_XF86AudioNext,            spawn,          {.v = audionext } },
	{ MODKEY,			XK_x,      spawn,          {.v = scrnlock } },
        { MODKEY,			XK_z,      spawn,          {.v = mem_sleep } },
	{ MODKEY,			XK_s,      spawn,          {.v = scrnshotsel } },
	{ MODKEY|ShiftMask,             XK_s,      spawn,          {.v = scrnshot } },
	{ MODKEY,                       XK_a,      spawn,          {.v = nnnfm } },
	{ MODKEY,                       XK_F3,     spawn,          {.v = bluctl } },
	{ MODKEY|ShiftMask,             XK_a,      spawn,          {.v = damb } },
	{ MODKEY,                       XK_w,      spawn,          {.v = web } },
	{ MODKEY|ShiftMask,             XK_w,      spawn,          {.v = webalt } },
	{ MODKEY,	                XK_n,      spawn,          {.v = ncmpcpp } },
	{ MODKEY,	                XK_y,      spawn,          {.v = ytsearch } },
	{ MODKEY|ShiftMask,             XK_y,      spawn,          {.v = ytsub } },
	{ MODKEY,                       XK_o,      spawn,          {.v = sticker } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

